#    AsciiArtCompressor
#    Copyright (C) 2020  Zachary Berkowitz
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
from bitarray import bitarray


def encode(data):
    data = data.replace(" ", "0")
    data = data.replace("#", "1")
    raw = [(data[i:i+2]) for i in range(0, len(data), 2)]
    enc = []
    numzeros = 0

    # find if data starts with rle or data packet
    if raw[0] == '00':
        ptype = "rle"
        enc.append("0")
    else:
        ptype = "data"
        enc.append("1")

    # encode data
    while len(raw) > 0:
        if ptype == "rle":
            if raw[0] == "00":
                notstop = True
                numzeros += 1
                del(raw[0])
            else:
                packet = genPacket(numzeros)
                enc.append(packet)
                numzeros = 0
                notstop = False
                enc.append(raw.pop(0))
                ptype = "data"
        else:
            if raw[0] == "00":
                enc.append("00")
                numzeros += 1
                del(raw[0])
                ptype = "rle"
            else:
                enc.append(raw.pop(0))
    if ptype == "rle" and notstop:
        packet = ""
        packet = genPacket(numzeros)
        enc.append(packet)
        numzeros = 0
    elif ptype == "data":
        enc.append("00")
            
    return enc

def genPacket(numzeros):
    bit = 0
    bits = ""
    bitsnum = 0
    bitsarray = []
    bitsarraystr = ""
    bitsnumarray = []
    bitsnumarraystr = ""
    n = numzeros + 1
    packet = ""
    while n > 0:
        bit = n % 2
        bitsnum += 1
        bitsarray.append(str(int(bit)))
        if bit == 1:
            n -= 1
        n /= 2
    while bitsnum > 2:
        bitsnumarray.append("1")
        bitsnum -= 1
    bitsnumarray.append("0")
    del(bitsarray[len(bitsarray) - 1])
    for bits in bitsarray:
        bitsarraystr = bits + bitsarraystr
    bits = ""
    for bits in bitsnumarray:
        bitsnumarraystr = bitsnumarraystr + bits
    packet = bitsnumarraystr + bitsarraystr
    return packet
    

def main():
    with open(sys.argv[1], 'r') as f:
        params = f.read().replace('\n', '|')

    params = params.split("|")

    outFile = 'encoded.bin'
    
    inFile = params[0]
    inFile= inFile.replace("File-", "")
    
    cols = params[1]
    cols = cols.replace("Columns-", "")
    
    with open(inFile, 'r') as file:
        data = file.read().replace('\n', '')
        
    colsint = "{0:b}".format(int(cols))
    colbytelen = len(colsint)
    colsint = ("0" * (8 - colbytelen)) + colsint
    
    print("Encode!")
    # encode the data
    edata = encode(data)

    barray = bitarray(colsint)
    for entry in edata:
        barray += entry

    print(barray)
    
    # write bitarray to file
    with open(outFile, 'wb') as f:
        f.write(barray)
    
    print("Encoded data written to %s" % outFile)

# call main 
if __name__ == '__main__': 
    main() 
