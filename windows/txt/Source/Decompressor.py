#    AsciiArtCompressor
#    Copyright (C) 2020  Zachary Berkowitz
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, argparse

def decode(barray, verbose):
    dec = []
    i = 0
    bitnum = 0
    tempnum = []
    numpair = ""
    string = ""
    data = ""
    rlen = True
    bstr = str(barray)
    raw = [*bstr]

    if raw[0] == "0":
        ptype = "rle"
        del(raw[0])
    else:
        ptype = "data"
        del(raw[0])

    while len(raw) > 0:
        if ptype == "rle" and rlen == True:
            if raw[0] == "1":
                bitnum += 1
                del(raw[0])
            else:
                bitnum += 1
                del(raw[0])
                rlen = False
        elif ptype == "rle" and rlen == False:
            tempnum = []
            string = ""
            data = ""
            tempnum.append("1")
            for i in range(bitnum):
                tempnum.append(raw.pop(0))
            for piece in tempnum:
                string = string + piece
            data = str(genData(string))
            dec.append(data)
            bitnum = 0
            ptype = "data"
            print("data")
        else:
            numpair = raw.pop(0) + raw.pop(0)
            print(numpair)
            if numpair == "00":
                ptype = "rle"
                rlen = True
                bitnum = 0
                print("rle")
            else:
                dec.append(numpair)
            numpair = ""
                
    print(dec)
    return dec

def genData(string):
    print(string)
    data = ""
    numpairs = 0
    numpairs = int(string, 2)
    for i in range(numpairs - 1):
        data = data + "00"
    return data

def main(): 
    # set input file 
    inFile = sys.argv[1]
        
    # set output file 
    outFile = 'decoded.txt'

    barray = ""

    # read bits from file
    with open(inFile, 'r') as f:
        barray = f.read()

    verbose = False

    if(verbose):
        print(barray)
    
    barray = barray.split("|")
    
    if len(barray) != 2:
        sys.exit("Invalid encoded format! Are the column number and binary number separated by '|' and nothing else?")
    
    cols = int(barray[0])
    print(cols)
    
    binary = barray[1]

    bstr = decode(binary, verbose)

    data=""
    for entry in bstr:
        data = data + entry
    data = data.replace("0", " ")
    data = data.replace("1", "#")
    img = [(data[i:i+cols]) for i in range(0, len(data), cols)]

    f = open(outFile, 'w')
    for row in img:
        f.write(row + '\n')
    f.close()

# call main 
if __name__ == '__main__': 
    main() 
