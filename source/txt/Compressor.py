#    AsciiArtCompressor
#    Copyright (C) 2020  Zachary Berkowitz
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, argparse


def encode(data, verbose):
    data = data.replace(" ", "0")
    data = data.replace("#", "1")
    print(data)
    raw = [(data[i:i+2]) for i in range(0, len(data), 2)]
    enc = []
    numzeros = 0
    
    # print raw data if verbose
    if verbose:
        print(raw)

    # find if data starts with rle or data packet
    if raw[0] == '00':
        print("rle")
        ptype = "rle"
        enc.append("0")
    else:
        print("data")
        ptype = "data"
        enc.append("1")

    # encode data
    while len(raw) > 0:
        if ptype == "rle":
            if raw[0] == "00":
                notstop = True
                numzeros += 1
                del(raw[0])
            else:
                print(numzeros)
                packet = genPacket(numzeros)
                enc.append(packet)
                numzeros = 0
                notstop = False
                enc.append(raw.pop(0))
                ptype = "data"
                print("data")
        else:
            if raw[0] == "00":
                enc.append("00")
                numzeros += 1
                del(raw[0])
                ptype = "rle"
                print("rle")
            else:
                enc.append(raw.pop(0))
    if ptype == "rle" and notstop:
        packet = ""
        print("END" + str(numzeros))
        packet = genPacket(numzeros)
        enc.append(packet)
        numzeros = 0
    elif ptype == "data":
        enc.append("00")
            
    return enc

def genPacket(numzeros):
    bit = 0
    bits = ""
    bitsnum = 0
    bitsarray = []
    bitsarraystr = ""
    bitsnumarray = []
    bitsnumarraystr = ""
    n = numzeros + 1
    packet = ""
    while n > 0:
        bit = n % 2
        bitsnum += 1
        bitsarray.append(str(int(bit)))
        if bit == 1:
            n -= 1
        n /= 2
    while bitsnum > 2:
        bitsnumarray.append("1")
        bitsnum -= 1
    bitsnumarray.append("0")
    del(bitsarray[len(bitsarray) - 1])
    for bits in bitsarray:
        bitsarraystr = bits + bitsarraystr
    bits = ""
    for bits in bitsnumarray:
        bitsnumarraystr = bitsnumarraystr + bits
    packet = bitsnumarraystr + bitsarraystr
    print(packet)
    return packet
    

def main(): 
    # create parser 
    parser = argparse.ArgumentParser()
    
    # add expected arguments 
    parser.add_argument('-c', '--columns', dest='cols', required=True)
    parser.add_argument('-f', '--file', dest='inFile', required=False) 
    parser.add_argument('-o', '--out', dest='outFile', required=False)
    parser.add_argument('-v', '--verbose', action='store_true', dest='verbose', default=None)

    # parse args
    args = parser.parse_args() 
    
    # set input file 
    inFile = 'image.txt'
    if args.inFile: 
        inFile = args.inFile
        
    # set output file 
    outFile = 'encoded.txt'
    if args.outFile: 
        outFile = args.outFile

    # show unencoded image
    with open('image.txt', 'r') as prev:
        print(prev.read())

    # read image file to string of data
    with open('image.txt', 'r') as file:
        data = file.read().replace('\n', '')

    print("Encode!")
    # encode the data
    edata = encode(data, args.verbose)

    bstr = str(args.cols) + "|"
    for entry in edata:
        bstr = bstr + entry

    print(bstr)
    
    # write bitarray to file
    with open(outFile, 'w') as f:
        f.write(bstr)
    
    print("Encoded data written to %s" % outFile)

# call main 
if __name__ == '__main__': 
    main() 
