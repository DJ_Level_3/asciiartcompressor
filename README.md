# AsciiArtCompressor

A tool to compress space-and-hashtag ASCII art files into pure binary



# Windows Usage:

**Compression:**
1. Open the windows folder
2. Enter the bin folder or the txt folder (bin for better compression, txt for readability)
3. Create image text file in the Compressor folder
4. Edit config.txt in the aforementioned folder to show the image text file name and the number of characters per row
5. Drag config.txt onto the .exe file to compress the image file
6. Compressed binary is in encoded.txt or encoded.bin

**Decompression:**
1. Open the windows folder
2. Enter the bin or txt folder, depending on your binary file type
3. Place the binary file in the Decompressor folder
4. Drag binary file onto the .exe to decompress the binary
5. Decompressed image is in decoded.txt



# Linux Usage:

**Dependencies:**
1. Python version 3
2. bitarray

**Compression:**
1. Open the linux folder
2. Enter the bin folder or the txt folder (bin for better compression, txt for readability)
3. Create image text file in the Compressor folder
5. Open the terminal in the Compressor folder
6. Run `python3 <Compressor or CompressorBin>.py -c <number of characters per row> [-f <image filename>]`
5. Compressed binary is in encoded.txt or encoded.bin

**Decompression:**
1. Open the linux folder
2. Enter the bin or txt folder, depending on your binary file type
3. Place the binary file in the Decompressor folder
5. Open the terminal in the Compressor folder
6. Run `python3 <Decompressor or DecompressorBin>.py [-f <binary filename>]`
5. Decompressed binary is in decoded.txt
